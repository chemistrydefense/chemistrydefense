﻿using Microsoft.Xna.Framework;
using System;
using Wobble;
using Wobble.Graphics;
using Wobble.Graphics.Sprites;
using Wobble.Input;
using Wobble.Screens;

namespace ChemistryDefense.Screens
{
    internal class StoryScreenView : ScreenView
    {
        private static readonly string StoryText =
@"  The evil corporation Blight found out that you wanna stop their plans of world domination, 
so they sent explosive boxes into your lab. You have to stop them from entering your main research area, 
by neutralising them with various chemical substances. To do this, you must create towers to facilitate 
the substances spread.

Click anywhere to continue...";

        public StoryScreenView(Screen screen) : base(screen)
        {
            var storyText = new SpriteText("cabin-medium", StoryText, 16, 1200)
            {
                Parent = Container,
                Alignment = Alignment.MidCenter,
                Tint = Color.White
            };
        }

        public override void Draw(GameTime gameTime)
        {
            GameBase.Game.GraphicsDevice.Clear(Color.Black);
            Container?.Draw(gameTime);
            try { GameBase.Game.SpriteBatch.End(); }
            catch (Exception) { }
        }

        public override void Update(GameTime gameTime)
        {
            Container?.Update(gameTime);
            if (MouseManager.IsUniqueClick(MouseButton.Left) || MouseManager.IsUniqueClick(MouseButton.Right))
                ScreenManager.ChangeScreen(new GameplayScreen());
        }

        public override void Destroy() => Container?.Destroy();

    }
}