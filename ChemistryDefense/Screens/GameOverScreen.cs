﻿using Wobble.Screens;

namespace ChemistryDefense.Screens
{
    class GameOverScreen : Screen
    {
        public sealed override ScreenView View { get; protected set; }

        public GameOverScreen() => View = new GameOverScreenView(this);
    }
}
