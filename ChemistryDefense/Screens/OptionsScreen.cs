﻿using Wobble.Screens;

namespace ChemistryDefense.Screens
{
    class OptionsScreen : Screen
    {
        public sealed override ScreenView View { get; protected set; }

        public OptionsScreen() => View = new OptionsScreenView(this);
    }
}
