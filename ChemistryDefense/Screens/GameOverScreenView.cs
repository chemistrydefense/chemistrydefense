﻿using Microsoft.Xna.Framework;
using System;
using Wobble;
using Wobble.Graphics;
using Wobble.Graphics.Sprites;
using Wobble.Input;
using Wobble.Screens;

namespace ChemistryDefense.Screens
{
    internal class GameOverScreenView : ScreenView
    {
        public GameOverScreenView(Screen screen) : base(screen)
        {
            var storyText = new SpriteText("cabin-medium", "Game Over\n  You Lose!", 50, 1200)
            {
                Parent = Container,
                Alignment = Alignment.MidCenter,
                Tint = Color.White
            };
        }

        public override void Draw(GameTime gameTime)
        {
            GameBase.Game.GraphicsDevice.Clear(Color.Black);
            Container?.Draw(gameTime);
            try { GameBase.Game.SpriteBatch.End(); }
            catch (Exception) { }
        }

        public override void Update(GameTime gameTime)
        {
            Container?.Update(gameTime);
            if (MouseManager.IsUniqueClick(MouseButton.Left) || MouseManager.IsUniqueClick(MouseButton.Right))
                ScreenManager.ChangeScreen(new MainMenuScreen());
        }

        public override void Destroy() => Container?.Destroy();

    }
}