﻿using Wobble.Screens;

namespace ChemistryDefense.Screens
{
    class MainMenuScreen : Screen
    {
        public sealed override ScreenView View { get; protected set; }

        public MainMenuScreen() => View = new MainMenuScreenView(this);
    }
}
