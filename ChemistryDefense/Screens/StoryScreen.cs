﻿using Wobble.Screens;

namespace ChemistryDefense.Screens
{
    class StoryScreen : Screen
    {
        public sealed override ScreenView View { get; protected set; }

        public StoryScreen() => View = new StoryScreenView(this);
    }
}
