using ChemistryDefense.Core;
using ChemistryDefense.Core.GameObjects;
using ChemistryDefense.Core.GameObjects.Containers;
using ChemistryDefense.Core.GameObjects.Elements;
using ChemistryDefense.Core.GameStates;
using ChemistryDefense.Core.Utilities;
using ChemistryDefense.Core.Utilities.Commands;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using Wobble;
using Wobble.Audio.Tracks;
using Wobble.Graphics;
using Wobble.Graphics.Sprites;
using Wobble.Graphics.Sprites.Text;
using Wobble.Graphics.UI.Buttons;
using Wobble.Graphics.UI.Form;
using Wobble.Input;
using Wobble.Managers;
using Wobble.Screens;

namespace ChemistryDefense.Screens
{
    class GameplayScreenView : ScreenView
    {
        private static readonly int StartingMoney = 500;
        private static readonly int StartingHealth = 25;

        private AudioTrack _music = null;

        private bool Paused = false;
        private State currentState;
        private State pausedState;
        private State playingState;

        private TowerContainer _towerContainer = new TowerContainer();
        private BoxContainer _boxContainer = new BoxContainer();
        private float _boxSpawningTime = 0f;
        private float _boxSpawningInterval = 0.75f;

        private float _boxAttackTime = 0f;
        private float _boxAttackInterval = 0.1f;

        private bool _creatingTower = false;
        private Type _elementToBeAdded = null;

        private SpriteText _statusText;
        private Scene _scene;

        private ConsoleCommandInvoker _commandInvoker = new ConsoleCommandInvoker();

        public GameplayScreenView(Screen screen) : base(screen)
        {
            StartMusic();
            GenerateScene();
            SetupStates();
            AddButtons();
            AddInfoUI();
            AddConsole();
        }

        private void AddConsole()
        {
            var wobbleFontStore = new WobbleFontStore(1, GameBase.Game.Resources.Get($"ChemistryDefense.Resources/Fonts/cabin-medium.ttf"));
            Textbox commandTextBox = new Textbox(new ScalableVector2(500, 36), wobbleFontStore, 16, "", "Type commands here...")
            {
                Parent = Container,
                Alignment = Alignment.TopCenter,
                Tint = Color.Black,
                Alpha = 0.75f,
                Focused = false
            };
            commandTextBox.OnSubmit = (command) =>
            {
                var result = _commandInvoker.Run(command);
                if(!result)
                {
                    _statusText.Text = "Invalid command";
                    _statusText.Tint = Color.Red;
                }
            };
        }

        private void AddInfoUI()
        {
            var infoIconSize = new ScalableVector2(48, 48);

            var coinsTexture = TextureManager.Load("ChemistryDefense.Resources/Textures/UI/coins.png");
            var healthTexture = TextureManager.Load("ChemistryDefense.Resources/Textures/UI/health.png");

            var infoContainer = new Container()
            {
                Parent = Container,
                Size = new ScalableVector2(120, 100),
                Alignment = Alignment.BotRight
            };

            var hpSprite = new Sprite()
            {
                Parent = infoContainer,
                Image = healthTexture,
                Size = infoIconSize,
                Alignment = Alignment.TopRight
            };

            var coinsSprite = new Sprite()
            {
                Parent = infoContainer,
                Image = coinsTexture,
                Size = infoIconSize,
                Alignment = Alignment.BotRight
            };

            var hpText = new SpriteText("cabin-medium", "0", 16)
            {
                Parent = infoContainer,
                Alignment = Alignment.MidLeft,
                Y = -infoIconSize.Y.Value / 2,
                Tint = Color.Black
            };
            HealthPoints.Instance.HealthChanged += (o, e) =>
            {
                hpText.Text = HealthPoints.Instance.Amount.ToString();
                if (HealthPoints.Instance.Amount <= 0)
                    ScreenManager.ChangeScreen(new GameOverScreen());
            };

            var coinsText = new SpriteText("cabin-medium", "0", 16)
            {
                Parent = infoContainer,
                Alignment = Alignment.MidLeft,
                Y = infoIconSize.Y.Value / 2,
                Tint = Color.Black
            };
            CoinWallet.Instance.MoneyChanged += (o, e) => coinsText.Text = CoinWallet.Instance.Amount.ToString();

            HealthPoints.Instance.Add(StartingHealth);
            CoinWallet.Instance.Add(StartingMoney);

            _statusText = new SpriteText("cabin-medium", "Welcome to Chemistry Defense!", 16, 500)
            {
                Parent = Container,
                Tint = Color.White,
                Y = 40,
                Alignment = Alignment.TopCenter
            };
        }

        private void StartMusic()
        {
            _music = new AudioTrack(GameBase.Game.Resources.Get("ChemistryDefense.Resources/Audio/BGM/gameplay.wav"));
            _music.Play();
        }

        private void GenerateScene()
        {
            _scene = new Scene(Container);
            _scene.Generate(Core.GameLevels.ELevel.level1, () =>
            {
                if (Paused)
                {
                    updateState(EStateOptions.Play);
                }
                else
                {
                    updateState(EStateOptions.Pause);
                }
            });
        }

        private void SetupStates()
        {
            pausedState = new PausedState();
            playingState = new PlayingState();
            SetState(playingState);
        }

        private void AddButtons()
        {
            var elementsSymbols = new List<string>() { "H", "C", "N", "O", "Mg", "K", "Mn", "Ag"};
            var elementsTypes = new List<Type>() { 
                typeof(HydrogenElement), 
                typeof(CarbonElement), 
                typeof(NitrogenElement), 
                typeof(OxygenElement), 
                typeof(MagnesiumElement), 
                typeof(PotassiumElement), 
                typeof(ManganeseElement), 
                typeof(SilverElement)
            };
            var elementsDictionary = elementsSymbols.Zip(elementsTypes, (k, v) => new { k, v })
              .ToDictionary(x => x.k, x => x.v);

            var buttonSize = new ScalableVector2(48, 48);
            var buttonCount = elementsSymbols.Count + 1;

            var buttonsContainer = new Container()
            {
                Parent = Container,
                Size = new ScalableVector2(buttonSize.X.Value * buttonCount, buttonSize.Y.Value),
                Alignment = Alignment.BotCenter
            };

            var elementIndex = 0;
            foreach(var elementPair in elementsDictionary)
            {
                var elementSymbol = elementPair.Key;
                var elementType = elementPair.Value;
                
                var buttonTexture = TextureManager.Load($"ChemistryDefense.Resources/Textures/Objects/Elements/{elementSymbol}.png");
                var button = new ImageButton(buttonTexture)
                {
                    Size = buttonSize,
                    Parent = buttonsContainer,
                    X = elementIndex * buttonSize.X.Value
                };

                var elementPrice = GetElementPrice(elementType);
                new SpriteText("cabin-medium", elementPrice.ToString(), 12)
                {
                    Parent = button,
                    Alignment = Alignment.BotCenter,
                    Tint = Color.DarkGoldenrod
                };

                button.Clicked += (o, e) =>
                {
                    _elementToBeAdded = elementType;
                };
                ++elementIndex;
            }
            
            var towerAddTexture = TextureManager.Load($"ChemistryDefense.Resources/Textures/UI/tower_add.png");
            var towerAddButton = new ImageButton(towerAddTexture)
            {
                Size = buttonSize,
                Parent = buttonsContainer,
                X = elementsSymbols.Count * buttonSize.X.Value
            };

            new SpriteText("cabin-medium", Tower.Price.ToString(), 12)
            {
                Parent = towerAddButton,
                Alignment = Alignment.BotCenter,
                Tint = Color.DarkGoldenrod
            };
            towerAddButton.Clicked += (o, e) => _creatingTower = true;
        }


        public override void Draw(GameTime gameTime)
        {
            GameBase.Game.GraphicsDevice.Clear(Color.LightGray);
            Container?.Draw(gameTime);
            try { GameBase.Game.SpriteBatch.End(); }
            catch (Exception) { }
        }

        public override void Update(GameTime gameTime)
        {
            HandleBoxesLogic(gameTime);
            HandleItemBuying();
            Container?.Update(gameTime);
        }

        public override void Destroy()
        {
            _music?.Stop();
            Container?.Destroy();
        }

        private void HandleBoxesLogic(GameTime gameTime)
        {
            if (!Paused)
            {
                _boxSpawningTime += (float)gameTime.ElapsedGameTime.TotalSeconds;
                _boxAttackTime += (float)gameTime.ElapsedGameTime.TotalSeconds;
                while (_boxSpawningTime >= _boxSpawningInterval)
                {
                    _boxContainer.CreateBox(Scene.LevelOneStartPosition.Item1, Scene.LevelOneStartPosition.Item2, Container);
                    _boxSpawningTime -= _boxSpawningInterval;
                }
                _boxContainer.MoveBoxes(gameTime);
                while (_boxAttackTime >= _boxAttackInterval)
                {
                    _towerContainer.CheckBoxes(_boxContainer);
                    _boxAttackTime -= _boxAttackInterval;
                }
            }
        }

        private void HandleItemBuying()
        {
            if (_creatingTower)
            {
                if (MouseManager.IsUniqueClick(MouseButton.Left))
                {
                    var X = MouseManager.CurrentState.X - Tower.Size.X.Value / 2;
                    var Y = MouseManager.CurrentState.Y - Tower.Size.Y.Value / 2;
                    if (!CheckIfCanPlaceTower(X, Y))
                    {
                        _statusText.Text = "Cannot place there!";
                        _statusText.Tint = Color.Red;
                    }
                    else if (CoinWallet.Instance.Remove(Tower.Price))
                    {
                        _towerContainer.CreateTowerAt(X, Y, Container);
                    }
                    else
                    {
                        _statusText.Text = "Not enough money!";
                        _statusText.Tint = Color.Red;
                    }
                    _creatingTower = false;
                }
            }
            if (_elementToBeAdded != null)
            {
                var tower = _towerContainer.CheckTowerAt(MouseManager.CurrentState.X, MouseManager.CurrentState.Y);
                if (MouseManager.IsUniqueClick(MouseButton.Left))
                {
                    if(tower != null)
                    {

                        if (CoinWallet.Instance.Remove(GetElementPrice(_elementToBeAdded)))
                        {
                            tower.AddElement(_elementToBeAdded);
                        }
                        else
                        {
                            _statusText.Text = "Not enough money!";
                            _statusText.Tint = Color.Red;
                        }
                    }
                    else
                    {
                        _statusText.Text = "Please select a tower!";
                        _statusText.Tint = Color.Red;
                    }
                    _elementToBeAdded = null;
                }
            }
            if (MouseManager.IsUniqueClick(MouseButton.Right))
            {
                _creatingTower = false;
                _elementToBeAdded = null;
            }
        }


        private int GetElementPrice(Type elementType)
        {
            if (elementType.GetField("Price") == null) return 0;
            return (int)elementType.GetField("Price").GetValue(null);
        }

        private bool CheckIfCanPlaceTower(float X, float Y)
        {
            if (Y <= 70 || Y >= 670) return false;
            var towerRectangle = new Rectangle((int)X, (int)Y, (int)Tower.Size.X.Value, (int)Tower.Size.Y.Value);
            var envRectangles = new List<Rectangle>()
            {
                new Rectangle(0, 190, 500, 120),
                new Rectangle(380, 190, 110, 490),
                new Rectangle(380, 550, 600, 130),
                new Rectangle(870, 190, 100, 490),
                new Rectangle(870, 190, 540, 110)
            };
            foreach(var envRectangle in envRectangles)
                if (towerRectangle.Intersects(envRectangle))
                    return false;
            return _towerContainer.CheckTowerAt(X, Y) == null;
        }

        void SetState(State state)
        {
            currentState = state;
        }

        void updateState(EStateOptions option)
        {
            switch (option)
            {
                case EStateOptions.Play:
                    {
                        if (Play())
                        {
                            Paused = false;
                            SetState(playingState);
                        }
                        else
                        {

                        }
                        break;
                    }
                case EStateOptions.Pause:
                    {
                        if (Pause())
                        {
                            Paused = true;
                            SetState(pausedState);
                        }
                        else
                        {

                        }
                        break;
                    }
            }
        }

        bool Play()
        {
            return currentState.Playing();
        }

        bool Pause()
        {
            return currentState.Paused();
        }
    }
}