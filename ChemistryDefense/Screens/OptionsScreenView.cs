﻿using Microsoft.Xna.Framework;
using System;
using Wobble;
using Wobble.Audio;
using Wobble.Graphics;
using Wobble.Graphics.Sprites;
using Wobble.Graphics.UI.Buttons;
using Wobble.Graphics.UI.Form;
using Wobble.Managers;
using Wobble.Screens;

namespace ChemistryDefense.Screens
{
    internal class OptionsScreenView : ScreenView
    {
        public OptionsScreenView(Screen screen) : base(screen)
        {
            ResizeContainer();
            AddControls();
        }

        private void ResizeContainer()
        {
            Container.Width *= 0.7f;
            Container.Alignment = Alignment.MidCenter;
        }

        private void AddControls()
        {
            var backButtonTexture = TextureManager.Load("ChemistryDefense.Resources/Textures/UI/back_button.png");
            var backButton = new ImageButton(backButtonTexture)
            {
                Parent = Container,
                Alignment = Alignment.BotLeft,
                X = 10,
                Y = -10,
                Size = new ScalableVector2(60, 80)
            };
            backButton.Clicked += (o, e) => {
                ScreenManager.RemoveScreen();
            };

            var progressBallTexture = TextureManager.Load("ChemistryDefense.Resources/Textures/UI/progress_ball.png");
            var sliderTexture = TextureManager.Load("ChemistryDefense.Resources/Textures/UI/slider.png");

            var volumeSlider = new Slider(ChemistryDefenseGame.MusicVolume, new Vector2(480, 15), progressBallTexture)
            {
                Image = sliderTexture,
                Parent = Container,
                Alignment = Alignment.TopCenter,
                Y = 120
            };

            var volumeLabel = new SpriteText("cabin-medium", "Volume", 20)
            {
                Parent = volumeSlider,
                Alignment = Alignment.TopLeft,
                Tint = Color.Black,
                Y = -volumeSlider.Height - 20
            };
        }

        public override void Draw(GameTime gameTime)
        {
            GameBase.Game.GraphicsDevice.Clear(Color.Coral);
            Container?.Draw(gameTime);
            try { GameBase.Game.SpriteBatch.End(); }
            catch (Exception) { }
        }

        public override void Update(GameTime gameTime) => Container?.Update(gameTime);

        public override void Destroy() => Container?.Destroy();

    }
}