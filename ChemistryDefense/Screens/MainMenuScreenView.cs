﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using Wobble;
using Wobble.Audio.Tracks;
using Wobble.Graphics;
using Wobble.Graphics.Sprites;
using Wobble.Graphics.UI.Buttons;
using Wobble.Managers;
using Wobble.Screens;

namespace ChemistryDefense.Screens
{
    class MainMenuScreenView : ScreenView
    {
        private static readonly ScalableVector2 ButtonSize = new ScalableVector2(180, 50);
        private static readonly float LogoWidth = 720;
        private static readonly float ButtonGap = 15;

        private Dictionary<string, TextButton> _buttons;

        private AudioTrack _music = null;

        public MainMenuScreenView(Screen screen) : base(screen)
        {
            _music = new AudioTrack(GameBase.Game.Resources.Get("ChemistryDefense.Resources/Audio/BGM/main_menu.wav"));
            _music.Play();
            AddLogo();
            CreateButtons();
        }
        
        private void AddLogo()
        {
            var logoTexture = TextureManager.Load("ChemistryDefense.Resources/Textures/UI/logo.png");
            var logoAspectRatio = logoTexture.Width / logoTexture.Height;

            var logoSprite = new Sprite()
            {
                Image = logoTexture,
                Parent = Container,
                Size = new ScalableVector2(LogoWidth, LogoWidth / logoAspectRatio),
                Y = 0.05f * Container.Height,
                Alignment = Alignment.TopCenter,
            };
        }

        private void CreateButtons()
        {
            var buttonTexture = TextureManager.Load("ChemistryDefense.Resources/Textures/UI/main_menu_button.png");

            var buttonsOffsetY = 180;

            var buttonTexts = new List<string>()
            {
                "Start game",
                "Options",
                "Exit"
            };

            _buttons = new Dictionary<string, TextButton>();

            var buttonsContainer = new Container()
            {
                Parent = Container,
                Size = new ScalableVector2(ButtonSize.X.Value, buttonTexts.Count * (ButtonSize.Y.Value + ButtonGap)),
                Alignment = Alignment.MidCenter,
                Y = buttonsOffsetY
            };

            for (var index = 0; index < buttonTexts.Count; ++index)
            {
                _buttons.Add(buttonTexts[index], new TextButton(buttonTexture, "cabin-medium", buttonTexts[index], 24)
                {
                    Parent = buttonsContainer,
                    Size = ButtonSize,
                    Text =
                    {
                        Tint = Color.Purple,
                    },
                    Y = index * (ButtonGap + ButtonSize.Y.Value) + ButtonGap
                });
            }

            _buttons["Start game"].Clicked += (o, e) => ScreenManager.ChangeScreen(new StoryScreen());
            _buttons["Options"].Clicked += (o, e) => ScreenManager.AddScreen(new OptionsScreen());
            _buttons["Exit"].Clicked += (o, e) => GameBase.Game.Exit();
        }

        public override void Draw(GameTime gameTime)
        {
            GameBase.Game.GraphicsDevice.Clear(Color.Coral);
            Container?.Draw(gameTime);
            try { GameBase.Game.SpriteBatch.End(); }
            catch (Exception) { }
        }

        public override void Update(GameTime gameTime) => Container?.Update(gameTime);

        public override void Destroy()
        {
            Container?.Destroy();
            _music?.Stop();
        }
    }
}
