﻿using Wobble.Screens;

namespace ChemistryDefense.Screens
{
    class GameplayScreen : Screen
    {
        public sealed override ScreenView View { get; protected set; }

        public GameplayScreen() => View = new GameplayScreenView(this);
    }
}
