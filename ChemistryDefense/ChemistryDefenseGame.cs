using ChemistryDefense.Screens;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using Wobble;
using Wobble.Audio;
using Wobble.Audio.Tracks;
using Wobble.Bindables;
using Wobble.Discord;
using Wobble.Discord.RPC;
using Wobble.Discord.RPC.Logging;
using Wobble.Graphics.BitmapFonts;
using Wobble.IO;
using Wobble.Screens;

namespace ChemistryDefense
{
    class ChemistryDefenseGame : WobbleGame
    {
        /// <inheritdoc />
        /// <summary>
        /// </summary>
        protected override bool IsReadyToUpdate { get; set; }

        private static readonly string DiscordClientId = "703393097081552906";

        public static BindableInt MusicVolume;

        /// <inheritdoc />
        /// <summary>
        ///     Allows the game to perform any initialization it needs to before starting to run.
        ///     This is where it can query for any required services and load any non-graphic
        ///     related content.  Calling base.Initialize will enumerate through any components
        ///     and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            Resources.AddStore(new DllResourceStore("ChemistryDefense.Resources.dll"));
            GlobalUserInterface.Cursor.Hide(0);
            IsMouseVisible = true;

            Graphics.PreferredBackBufferWidth  = 1366;
            Graphics.PreferredBackBufferHeight = 768;

            DiscordManager.CreateClient(DiscordClientId, LogLevel.None);
            DiscordManager.Client.SetPresence(new RichPresence()
            {
                State = "Playing",
                Timestamps = new Timestamps() { Start = DateTime.UtcNow },
                Assets = new Assets()
                {
                    LargeImageKey = "logo"
                }
            });

            base.Initialize();
        }

        /// <inheritdoc />
        /// <summary>
        ///     LoadContent will be called once per game and is the place to load
        ///     all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            base.LoadContent();

            // TODO: Your asset loading code goes here.
            LoadFonts();

            MusicVolume = new BindableInt(50, 0, 100, (o, e) => {
                AudioTrack.GlobalVolume = e.Value;
            });

            // Tell the game that it should start updating now
            IsReadyToUpdate = true;

            // TODO: Change to the first screen via ScreenManager
            ScreenManager.ChangeScreen(new MainMenuScreen());
        }

        private void LoadFonts()
        {
            var fontNames = new List<string>()
            {
                "cabin-medium"
            };

            foreach(var fontName in fontNames)
            {
                BitmapFontFactory.AddFont(fontName, GameBase.Game.Resources.Get($"ChemistryDefense.Resources/Fonts/{fontName}.ttf"));
            }
        }

        /// <inheritdoc />
        /// <summary>
        ///     UnloadContent will be called once per game and is the place to unload
        ///     game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            base.UnloadContent();

            // TODO: Your disposing logic goes here.
        }

        /// <inheritdoc />
        /// <summary>
        ///     Allows the game to run logic such as updating the world,
        ///     checking for collisions, gathering input, and playing audio.
        /// </summary>
        protected override void Update(GameTime gameTime)
        {
            if (!IsReadyToUpdate)
                return;

            base.Update(gameTime);

            // TODO: Your global update logic goes here. Anything updated here will be updated on every screen.
        }

        /// <inheritdoc />
        /// <summary>
        ///     This is called when the game should draw itself.
        /// </summary>
        protected override void Draw(GameTime gameTime)
        {
            if (!IsReadyToUpdate)
                return;

            base.Draw(gameTime);

            // TODO: Your global draw logic goes here. Anything drawn here will be drawn on top of every screen.
        }
    }
}
