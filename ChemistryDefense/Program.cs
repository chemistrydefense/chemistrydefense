﻿using System;

namespace ChemistryDefense
{
    internal static class Program
    {
        [STAThread]
        internal static void Main(string[] args)
        {
            using (var game = new ChemistryDefenseGame())
            {
                game.Run();
            }
        }
    }
}
