﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Text;
using Wobble.Graphics.Sprites;
using Wobble.Managers;

namespace ChemistryDefense.Core.GameObjects
{
    class TextureFactory
    {
        private static Dictionary<string, Texture2D> cachedTexture = new Dictionary<string, Texture2D>();

        public static Texture2D GetTexture(string texturePath)
        {
            if(!cachedTexture.TryGetValue(texturePath, out var texture))
            {
                texture = TextureManager.Load(texturePath);
                cachedTexture.Add(texturePath, texture);
            }

            return texture;
        }
    }
}
