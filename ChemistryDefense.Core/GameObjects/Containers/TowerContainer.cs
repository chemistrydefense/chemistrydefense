﻿using ChemistryDefense.Core.Utilities.Boxes;
using System;
using System.Collections.Generic;
using System.Numerics;
using Wobble.Graphics;

namespace ChemistryDefense.Core.GameObjects.Containers
{
    public class TowerContainer
    {
        private List<Tower> _towers;

        public TowerContainer()
        {
            _towers = new List<Tower>();
        }

        public Tower CheckTowerAt(float X, float Y)
        {
            var upperBound = new Vector2(X - Tower.Size.X.Value, Y - Tower.Size.Y.Value);
            var lowerBound = new Vector2(X, Y);
            foreach (var tower in _towers)
                if ((upperBound.X <= tower.X && tower.X <= lowerBound.X) && (upperBound.Y <= tower.Y && tower.Y <= lowerBound.Y))
                    return tower;
            return null;
        }

        public Tower CreateTowerAt(float X, float Y, Container parentGraphicsContainer)
        {
            var tower = new Tower(parentGraphicsContainer)
            {
                X = X,
                Y = Y
            };
            _towers.Add(tower);
            return tower;
        }

        public void CheckBoxes(BoxContainer boxContainer)
        {
            foreach(var tower in _towers)
            {
                var box = boxContainer.GetTargetedBox(tower);
                if(box != null)
                {
                    box.Damage(tower.GetInflictedDamage());
                }
            }
        }
    }
}
