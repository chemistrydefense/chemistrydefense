﻿using ChemistryDefense.Core.Utilities.Boxes;
using Microsoft.Xna.Framework;
using System.Collections.Generic;
using Wobble.Graphics;

namespace ChemistryDefense.Core.GameObjects.Containers
{
    public class BoxContainer
    {
        private List<Box> _boxes;
        private BoxTargetingSystem _boxTargetingSystem;

        private int _nextBoxHP = 10;
        private float _nextBoxSpeed = 100;

        private int _statsIncreaseBoxesCount = 10;
        private int _hpIncreaseAmount = 1;
        private float _speedIncreaseAmount = 0.25f;

        private int _boxSpawnIndex = 0;

        public BoxContainer()
        {
            _boxes = new List<Box>();
            _boxTargetingSystem = new BoxTargetingSystem();
            _boxTargetingSystem.ChangeStrategy(new NearestBoxStrategy());
        }

        public Box GetTargetedBox(Tower tower)
        {
            return _boxTargetingSystem.GetBox(tower, _boxes);
        }

        public void CreateBox(float X, float Y, Container parentContainer)
        {
            var box = new Box(parentContainer, new ScalableVector2(50, 50), speed: _nextBoxSpeed, hp: _nextBoxHP)
            {
                X = X,
                Y = Y,
            };
            box.Destroyed += (o, e) => _boxes.Remove(box);
            _boxes.Add(box);
            ++_boxSpawnIndex;
            if(_boxSpawnIndex % _statsIncreaseBoxesCount == 0)
            {
                _nextBoxSpeed += _speedIncreaseAmount;
                _nextBoxHP += _hpIncreaseAmount;
            }
        }

        public void MoveBoxes(GameTime gameTime)
        {
            foreach (var box in _boxes)
            {
                box.Move((float)(box.Speed * gameTime.ElapsedGameTime.TotalSeconds));
                if (box.X > 430 && box.X < 450 && box.Y > 190 && box.Y < 210)
                {
                    box.Direction = EDiraction.Down;
                }
                else if (box.X > 430 && box.X < 450 && box.Y > 550 && box.Y < 600)
                {
                    box.Direction = EDiraction.Right;
                }
                else if (box.X > 890 && box.X < 910 && box.Y > 550 && box.Y < 600)
                {
                    box.Direction = EDiraction.Up;
                }
                else if (box.X > 890 && box.X < 910 && box.Y > 190 && box.Y < 210)
                {
                    box.Direction = EDiraction.Right;
                }
            }
            for (var index = _boxes.Count - 1; index >= 0; --index)
                _boxes[index].CheckOffScreen();
        }
    }
}
