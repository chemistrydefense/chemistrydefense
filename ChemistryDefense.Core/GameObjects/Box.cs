﻿using ChemistryDefense.Core.Utilities;
using Microsoft.Xna.Framework;
using System;
using Wobble.Graphics;
using Wobble.Graphics.Sprites;

namespace ChemistryDefense.Core.GameObjects
{
    public class Box : GameObject, IBox
    {
        private static readonly string TexturePath = "ChemistryDefense.Resources/Textures/Objects/box.png";

        private float _hp;
        public EDiraction Direction { get; set; }
        public float Speed { get; }

        private SpriteText _hpText;

        public float MaxHP { get; private set; }

        public float HP
        {
            get => _hp;
            private set
            {
                _hp = value;

                if (_hp > MaxHP)
                    MaxHP = _hp;

                if (_hp <= 0)
                {
                    Destroy();
                    CoinWallet.Instance.Add((int)MaxHP);
                }
            }
        }

        public Box(Container parentContainer, ScalableVector2 size, float hp = 100, float speed = 10, EDiraction direction = EDiraction.Right)
            : base(TexturePath, parentContainer, size)
        {
            MaxHP = hp;

            HP = hp;
            Speed = speed;
            Direction = direction;

            _hpText = new SpriteText("cabin-medium", HP.ToString(), 12)
            {
                Parent = _sprite,
                Alignment = Alignment.BotCenter,
                Y = 10,
                Tint = Color.Blue
            };
        }

        public void Damage(float amount)
        {
            HP -= amount;
            _hpText.Text = HP.ToString();
        }

        public void Move(float amount)
        {
            switch (Direction)
            {
                case EDiraction.Up:
                    {
                        Y -= amount;
                        break;
                    }
                case EDiraction.Down:
                    {
                        Y += amount;
                        break;
                    }
                case EDiraction.Left:
                    {
                        X -= amount;
                        break;
                    }
                case EDiraction.Right:
                    {
                        X += amount;
                        break;
                    }
            }
        }

        internal void CheckOffScreen()
        {
            if (X > 1366)
            {
                Destroy();
                HealthPoints.Instance.Damage(1);
            }
        }
    }
}
