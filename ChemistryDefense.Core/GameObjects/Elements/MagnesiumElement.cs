﻿namespace ChemistryDefense.Core.GameObjects.Elements
{
    public class MagnesiumElement : ElementDecorator
    {
        private static readonly float MagnesiumDamage = 5.0f;

        public override string Symbol => "Mg";

        public static readonly int Price = 400;

        public MagnesiumElement(IElement element) : base(element)
        {
        }

        public override float GetInflictedDamage()
        {
            return base.GetInflictedDamage() + MagnesiumDamage;
        }
    }
}
