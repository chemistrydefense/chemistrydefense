﻿namespace ChemistryDefense.Core.GameObjects.Elements
{
    public class ManganeseElement : ElementDecorator
    {
        private static readonly float ManganeseDamage = 8.0f;

        public override string Symbol => "Mn";

        public static readonly int Price = 600;

        public ManganeseElement(IElement element) : base(element)
        {
        }

        public override float GetInflictedDamage()
        {
            return base.GetInflictedDamage() + ManganeseDamage;
        }
    }
}
