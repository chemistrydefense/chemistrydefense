﻿namespace ChemistryDefense.Core.GameObjects.Elements
{
    public class SilverElement : ElementDecorator
    {
        private static readonly float SilverDamage = 12.0f;

        public override string Symbol => "Ag";

        public static readonly int Price = 1000;

        public SilverElement(IElement element) : base(element)
        {
        }

        public override float GetInflictedDamage()
        {
            return base.GetInflictedDamage() + SilverDamage;
        }
    }
}
