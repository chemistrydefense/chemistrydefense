﻿namespace ChemistryDefense.Core.GameObjects.Elements
{
    public class HydrogenElement : ElementDecorator
    {
        private static readonly float HydrogenDamage = 1.0f;

        public override string Symbol => "H";

        public static readonly int Price = 100;

        public HydrogenElement(IElement element) : base(element)
        {
        }

        public override float GetInflictedDamage()
        {
            return base.GetInflictedDamage() + HydrogenDamage;
        }
    }
}
