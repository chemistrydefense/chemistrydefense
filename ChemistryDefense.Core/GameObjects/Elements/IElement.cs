﻿using System;
using System.Collections.Generic;

namespace ChemistryDefense.Core.GameObjects.Elements
{
    public interface IElement
    {
        float GetInflictedDamage();

        string Symbol { get; }

        List<string> StackedElements { get; }
    }
}