﻿namespace ChemistryDefense.Core.GameObjects.Elements
{
    public class PotassiumElement : ElementDecorator
    {
        private static readonly float PotassiumDamage = 6.0f;

        public override string Symbol => "K";

        public static readonly int Price = 430;

        public PotassiumElement(IElement element) : base(element)
        {
        }

        public override float GetInflictedDamage()
        {
            return base.GetInflictedDamage() + PotassiumDamage;
        }
    }
}
