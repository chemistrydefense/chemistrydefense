﻿using System.Collections.Generic;

namespace ChemistryDefense.Core.GameObjects.Elements
{
    public abstract class ElementDecorator : IElement
    {
        private IElement _element;

        public ElementDecorator(IElement element)
        {
            _element = element;
        }

        public virtual float GetInflictedDamage()
        {
            return _element.GetInflictedDamage();
        }

        public List<string> StackedElements {
            get
            {
                var elements = new List<string> { Symbol };
                elements.AddRange(_element.StackedElements);
                return elements;
            }
        }

        public virtual string Symbol => "-";
    }
}
