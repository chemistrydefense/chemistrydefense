﻿namespace ChemistryDefense.Core.GameObjects.Elements
{
    public class NitrogenElement : ElementDecorator
    {
        private static readonly float NitrogenDamage = 3.5f;

        public override string Symbol => "N";
        public static readonly int Price = 280;

        public NitrogenElement(IElement element) : base(element)
        {
        }

        public override float GetInflictedDamage()
        {
            return base.GetInflictedDamage() + NitrogenDamage;
        }
    }
}
