﻿namespace ChemistryDefense.Core.GameObjects.Elements
{
    public class OxygenElement : ElementDecorator
    {
        private static readonly float OxygenDamage = 4.0f;
        public override string Symbol => "O";

        public static readonly int Price = 300;

        public OxygenElement(IElement element) : base(element)
        {
        }

        public override float GetInflictedDamage()
        {
            return base.GetInflictedDamage() + OxygenDamage;
        }
    }
}
