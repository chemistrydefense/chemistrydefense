﻿namespace ChemistryDefense.Core.GameObjects.Elements
{
    public class CarbonElement : ElementDecorator
    {
        private static readonly float CarbonDamage = 2.0f;

        public override string Symbol => "C";

        public static readonly int Price = 180;

        public CarbonElement(IElement element) : base(element)
        {
        }

        public override float GetInflictedDamage()
        {
            return base.GetInflictedDamage() + CarbonDamage;
        }
    }
}
