﻿using System.Collections.Generic;

namespace ChemistryDefense.Core.GameObjects.Elements
{
    public class BaseElement : IElement
    {
        public static readonly float BasicDamageAmount = 1;

        public string Symbol => "-";

        public List<string> StackedElements => new List<string>();

        public float GetInflictedDamage()
        {
            return BasicDamageAmount;
        }
    }
}
