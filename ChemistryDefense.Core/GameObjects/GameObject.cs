﻿using System;
using System.Collections.Generic;
using System.Text;
using Wobble.Graphics;
using Wobble.Graphics.Sprites;

namespace ChemistryDefense.Core.GameObjects
{
    public abstract class GameObject
    {
        protected Sprite _sprite;

        public EventHandler<EventArgs> Destroyed;
        public float X
        {
            get => _sprite.X;
            set => _sprite.X = value;
        }

        public float Y
        {
            get => _sprite.Y;
            set => _sprite.Y = value;
        }

        public float Rotation
        {
            get => _sprite.Rotation;
            set => _sprite.Rotation = value;
        }

        public GameObject(string resaurcesTexturePath, Container parentContainer, ScalableVector2 size)
        {
            _sprite = new Sprite()
            {
                Image = TextureFactory.GetTexture(resaurcesTexturePath),
                Parent = parentContainer,
                Size = size
            };
        }

        protected virtual void Destroy()
        {
            _sprite.Destroy();
            Destroyed?.Invoke(this, null);
        }
    }
}
