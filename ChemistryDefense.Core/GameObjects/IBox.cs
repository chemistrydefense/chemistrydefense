﻿namespace ChemistryDefense.Core.GameObjects
{
    public interface IBox
    {
        float HP { get; }

        void Damage(float amount);
    }
}