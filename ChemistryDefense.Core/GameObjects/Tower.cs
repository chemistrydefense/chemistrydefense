﻿using ChemistryDefense.Core.GameObjects.Elements;
using ChemistryDefense.Core.Utilities;
using System;
using System.Collections.Generic;
using System.Numerics;
using Wobble.Graphics;
using Wobble.Graphics.Sprites;
using Wobble.Managers;

namespace ChemistryDefense.Core.GameObjects
{
    public class Tower : GameObject
    {
        public static readonly int MaxElementsCount = 5;
        public bool ElementsLocked { get; set; } = false;

        private static readonly string TexturePath = "ChemistryDefense.Resources/Textures/Objects/tower.png";

        public static readonly ScalableVector2 Size = new ScalableVector2(90, 84);

        public static int Price = 250;

        public static int Range { get; } = 200;

        private IElement _topElement = new BaseElement();
        private IElement TopElement
        {
            get => _topElement;
            set
            {
                _topElement = value;
                UpdateElementsView();
            }
        }

        public List<string> Elements {
            get
            {
                if (TopElement == null)
                    return new List<string>();
                var elements = TopElement.StackedElements;
                elements.Reverse();
                return elements;
            } 
        }

        public Tower(Container parentContainer) : base(TexturePath, parentContainer, Size)
        {
        }

        private void UpdateElementsView()
        {
            var elementSize = new ScalableVector2(12, 12);
            var elementsGap = 5;

            var elementsOffset = new Vector2(5, 70);

            _sprite.Children.Clear();
            var elements = Elements;
            for(var index = 0; index < elements.Count; ++index)
            {
                new Sprite()
                {
                    Image = TextureManager.Load($"ChemistryDefense.Resources/Textures/Objects/Elements/{elements[index]}.png"),
                    Parent = _sprite,
                    Size = elementSize,
                    X = elementsOffset.X + index * (elementSize.X.Value + elementsGap),
                    Y = elementsOffset.Y
                };
            }
        }

        public bool AddElement(Type elementType)
        {
            if (elementType.BaseType != typeof(ElementDecorator))
                throw new ArgumentException();
            if (ElementsLocked || Elements.Count >= MaxElementsCount)
                return false;
            TopElement = (IElement)elementType.GetConstructor(new[] { typeof(IElement) }).Invoke(new[] { TopElement });
            return true;
        }

        public bool InRange(float X, float Y)
        {
            return MathUtils.GetEuclideanDistance(X, Y, this.X, this.Y) <= Range;
        }

        public float GetInflictedDamage()
        {
            return TopElement.GetInflictedDamage();
        }

    }
}
