﻿using ChemistryDefense.Core.GameObjects;
using System.Collections.Generic;
using System.Linq;

namespace ChemistryDefense.Core.Utilities.Boxes
{
    public class LastBoxStrategy : IBoxTargetingStrategy
    {
        public Box GetBox(Tower tower, IEnumerable<Box> boxes)
        {
            var availableBoxes = boxes.Reverse().ToList().FindAll(box => tower.InRange(box.X, box.Y));
            if (availableBoxes.Count == 0) return null;
            return availableBoxes.First();
        }
    }
}
