﻿using ChemistryDefense.Core.GameObjects;
using System.Collections.Generic;

namespace ChemistryDefense.Core.Utilities.Boxes
{
    public interface IBoxTargetingStrategy
    {
        Box GetBox(Tower tower, IEnumerable<Box> boxes);
    }
}
