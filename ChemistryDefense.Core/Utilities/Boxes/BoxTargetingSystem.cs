﻿using ChemistryDefense.Core.GameObjects;
using System.Collections.Generic;

namespace ChemistryDefense.Core.Utilities.Boxes
{
    public class BoxTargetingSystem
    {
        private IBoxTargetingStrategy _strategy = null;

        public Box GetBox(Tower tower, IEnumerable<Box> boxes)
        {
            if (_strategy == null)
                return null;
            return _strategy.GetBox(tower, boxes);
        }

        public void ChangeStrategy(IBoxTargetingStrategy newStrategy)
        {
            _strategy = newStrategy;
        }
    }
}
