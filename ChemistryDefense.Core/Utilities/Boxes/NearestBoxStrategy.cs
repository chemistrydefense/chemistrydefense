﻿ using ChemistryDefense.Core.GameObjects;
using System.Collections.Generic;
using System.Linq;

namespace ChemistryDefense.Core.Utilities.Boxes
{
    public class NearestBoxStrategy : IBoxTargetingStrategy
    {
        public Box GetBox(Tower tower, IEnumerable<Box> boxes)
        {
            var orderedList = boxes.OrderBy(box => MathUtils.GetEuclideanDistance(box.X, box.Y, tower.X, tower.Y)).ToList().FindAll(box => tower.InRange(box.X, box.Y));
            if (orderedList.Count == 0) return null;
            return orderedList.First();
        }
    }
}
