﻿using System;

namespace ChemistryDefense.Core.Utilities
{
    public sealed class CoinWallet
    {
        private static CoinWallet _instance =  null;
        public int Amount { get; private set; }
        private CoinWallet() { }

        public EventHandler<EventArgs> MoneyChanged;

        public static CoinWallet Instance
        {
            get { 
                if(_instance == null)
                {
                    _instance = new CoinWallet();
                }
                return _instance;
            }
        }

        public void Add(int coins)
        {
            Amount += coins;
            MoneyChanged?.Invoke(this, null);
        }

        public bool Remove(int coins)
        {
            if(Amount < coins)
            {
                return false;
            }
            Amount -= coins;
            MoneyChanged?.Invoke(this, null);
            return true;
        }
    }
}
