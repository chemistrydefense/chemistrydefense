﻿using System;

namespace ChemistryDefense.Core.Utilities
{
    public sealed class HealthPoints
    {
        private static HealthPoints _instance = null;
        public int Amount { get; private set; }
        private HealthPoints() { }

        public EventHandler<EventArgs> HealthChanged;

        public static HealthPoints Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new HealthPoints();
                }
                return _instance;
            }
        }
        public void Add(int points)
        {
            Amount += points;
            HealthChanged?.Invoke(this, null);
        }

        public void Damage(int damage)
        {
            if (Amount < damage)
                Amount = 0;
            Amount -= damage;
            HealthChanged?.Invoke(this, null);
        }
    }
}
