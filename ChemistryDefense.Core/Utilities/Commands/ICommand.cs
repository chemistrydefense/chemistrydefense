﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChemistryDefense.Core.Utilities.Commands
{
    public interface ICommand
    {
        string Name { get; set; }
        List<string> Args { get; set; }
        public bool Execute();
    };
}
