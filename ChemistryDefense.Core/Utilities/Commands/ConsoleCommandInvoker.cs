﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChemistryDefense.Core.Utilities.Commands
{
    public class ConsoleCommandInvoker
    {
        public bool Run(string fullCommand)
        {
            var words = fullCommand.Split(' ').ToList();
            var consoleCmd = new ConsoleCommand(words.First(), words.GetRange(1, words.Count() - 1));
            return consoleCmd.Execute();
        }
    }
}
