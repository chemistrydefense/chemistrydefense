﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChemistryDefense.Core.Utilities.Commands
{
    public class ConsoleCommand : ICommand
    {
        public string Name { get; set; }
        public List<string> Args { get; set; }

        public ConsoleCommand(string name, List<string> args)
        {
            Name = name;
            Args = args;
        }
        public bool Execute() 
        {
            switch (Name)
            {
                case "add_gold":
                    { 
                        if (Args.Count != 1) { return false; }
                        int amount;
                        var result = int.TryParse(Args[0], out amount);
                        if (!result) return false;
                        CoinWallet.Instance.Add(amount);
                        return true;
                    }
                case "add_health":
                    {
                        if (Args.Count != 1) { return false; }
                        int amount;
                        var result = int.TryParse(Args[0], out amount);
                        if (!result) return false;
                        HealthPoints.Instance.Add(amount);
                        return true;
                    }
                default:
                    return false;
            }
        }
    }
}
