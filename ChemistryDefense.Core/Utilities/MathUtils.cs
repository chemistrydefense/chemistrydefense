﻿using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;

namespace ChemistryDefense.Core.Utilities
{
    static class MathUtils
    {
        public static float GetEuclideanDistance(float x1, float y1, float x2, float y2)
        {
            return Vector2.Distance(new Vector2(x1, y1), new Vector2(x2, y2));
        }
    }
}
