﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChemistryDefense.Core.GameStates
{
    public class PlayingState : State
    {
        public override bool Paused() => true;
        public override bool Playing() => false;
    }
}
