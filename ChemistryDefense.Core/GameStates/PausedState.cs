﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChemistryDefense.Core.GameStates
{
    public class PausedState : State
    {
        public override bool Paused() => false;
        public override bool Playing() => true;
    }
}
