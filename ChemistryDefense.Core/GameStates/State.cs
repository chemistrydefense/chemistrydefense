﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChemistryDefense.Core.GameStates
{
    public abstract class State
    {
        public abstract bool Paused();
        public abstract bool Playing();
    }
}
