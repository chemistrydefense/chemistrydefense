﻿using ChemistryDefense.Core.GameLevels;
using ChemistryDefense.Core.GameObjects;
using System;
using System.Collections.Generic;
using System.Text;
using Wobble.Graphics;
using Wobble.Graphics.UI.Buttons;
using Wobble.Managers;

namespace ChemistryDefense.Core
{
    public class Scene : LevelGenerator
    {
        public static Tuple<float, float> LevelOneStartPosition = new Tuple<float, float>(-50, 200);

        private List<EnvironmentObject> _environmentObjects = new List<EnvironmentObject>();
        private Container _parentContainer;

        public Scene(Container parentContainer)
        {
            _parentContainer = parentContainer;
        }

        public void Generate(ELevel level, Action pauseAction)
        {
            var buttonTexture = TextureManager.Load("ChemistryDefense.Resources/Textures/UI/PlayPause.png");

            var playPauseButton = new ImageButton(buttonTexture)
            {
                Parent = _parentContainer,
                Size = new ScalableVector2(50, 50),
                X = 20,
                Y = 20
            };
            
            playPauseButton.Clicked += (o, e) => pauseAction();

            switch (level)
            {
                case ELevel.level1:
                    {
                        GenerateLevelOne(_environmentObjects, _parentContainer);
                        break;
                    }
            }
        }
    }
}
