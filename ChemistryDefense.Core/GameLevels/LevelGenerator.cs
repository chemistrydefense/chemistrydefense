﻿using ChemistryDefense.Core.GameObjects;
using System;
using System.Collections.Generic;
using System.Numerics;
using Wobble.Graphics;

namespace ChemistryDefense.Core.GameLevels
{
    public class LevelGenerator
    {
        protected void GenerateLevelOne(List<EnvironmentObject> envObjects, Container parentContainer)
        {
            List<Tuple<Vector2, float>> railPositions = new List<Tuple<Vector2, float>>()
            {
                new Tuple<Vector2, float>(new Vector2(0, 0), 0),
                new Tuple<Vector2, float>(new Vector2(230, 180), 90),
                new Tuple<Vector2, float>(new Vector2(480, 360), 0),
                new Tuple<Vector2, float>(new Vector2(740, 180), -90),
                new Tuple<Vector2, float>(new Vector2(960, 0), 0)
            };

            var factoryLineSize = new ScalableVector2(500, 120);
            var factoryLineTexture = "ChemistryDefense.Resources/Textures/Objects/factory_line.png";

            foreach (var item in railPositions)
            {
                var position = item.Item1;
                var rotation = item.Item2;

                var envObject = new EnvironmentObject(factoryLineTexture, parentContainer, factoryLineSize)
                {
                    X = Scene.LevelOneStartPosition.Item1 + position.X,
                    Y = Scene.LevelOneStartPosition.Item2 + position.Y,
                    Rotation = rotation
                };
                envObjects.Add(envObject);
            }
        }
    }
}
