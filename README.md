
# Chemistry Defense
## How to build
1. Clone the repo
2. Run the following command inside the repo
```
git submodule update --init --recursive
```
## Used designs patterns
### Creational
#### Singleton
![singleton](design_patterns/singleton.png)

### Structural
#### Flyweight
![flyweight](design_patterns/flyweight.png)

#### Decorator
![decorator](design_patterns/decorator.png)

### Behavioural
#### Command
![command](design_patterns/command.png)

#### State
![state](design_patterns/state.png)

#### Strategy
![strategy](design_patterns/strategy.png)